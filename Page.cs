﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace TestProject
{
    public class Page
    {
        IWebDriver driver;
        public Page(IWebDriver _driver)
        {
            driver = _driver;
        }

        public By loginFieldTextBox = By.XPath("/html/body/div/div/div[2]/div[1]/div/div/form/div[1]/input");
        public By passwordFieldTextBox = By.XPath("/html/body/div/div/div[2]/div[1]/div/div/form/div[2]/input");
        public By submitButton = By.XPath("/html/body/div/div/div[2]/div[1]/div/div/form/input");
        public By errorMessageTextBox = By.XPath("/html/body/div/div/div[2]/div[1]/div/div/form/div[3]/h3/text()");
        public By LogoTitleTextBox = By.XPath("/html/body/div/div/div/div[1]/div[1]/div[2]/div");
    }
}
