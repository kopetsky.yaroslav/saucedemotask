using Newtonsoft.Json.Bson;
using OpenQA.Selenium;

namespace TestProject
{
    public class Tests
    {
        private IWebDriver driver;
        private Page page;
        [SetUp]
        public void Setup()
        {
            driver = new OpenQA.Selenium.Chrome.ChromeDriver("C:\\Program Files(x86)\\Google\\Chrome\\Application\\chrome.exe");
            page = new Page(driver);
            driver.Navigate().GoToUrl("https://www.saucedemo.com/");
            driver.Manage().Window.Maximize();
        }

        [Test]
        public void UC1()
        {   
            var _loginField = driver.FindElement(page.loginFieldTextBox);
            var _passwordField = driver.FindElement(page.passwordFieldTextBox);
            var _submitButton = driver.FindElement(page.submitButton);
            var _errorMessageTextBox = driver.FindElement(page.errorMessageTextBox);
            _loginField.Click();
            _loginField.SendKeys("testtest1");

            
            _loginField.Clear();

            _passwordField.Click();
            _passwordField.SendKeys("testtest1");
            _passwordField.Clear();

            _submitButton.Click();

            if (_errorMessageTextBox.Text == "Epic sadface: Username is required")
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail();
            }

      
        }

        [Test]
        public void UC2()
        {
            var _loginField = driver.FindElement(page.loginFieldTextBox);
            var _passwordField = driver.FindElement(page.passwordFieldTextBox);
            var _submitButton = driver.FindElement(page.submitButton);
            var _errorMessageTextBox = driver.FindElement(page.errorMessageTextBox);
            _loginField.Click();
            _loginField.SendKeys("standard_user");


            _submitButton.Click();

            if (_errorMessageTextBox.Text == "Epic sadface: Password is required")
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail();
            }


        }

        [Test]
        public void UC3()
        {
            var _loginField = driver.FindElement(page.loginFieldTextBox);
            var _passwordField = driver.FindElement(page.passwordFieldTextBox);
            var _submitButton = driver.FindElement(page.submitButton);
            var _errorMessageTextBox = driver.FindElement(page.errorMessageTextBox);
            _loginField.Click();
            _loginField.SendKeys("problem_user");


      

            _passwordField.Click();
            _passwordField.SendKeys("secret_sauce");
       

            _submitButton.Click();

            var _titleText = driver.FindElement(page.LogoTitleTextBox);

            if (_titleText.Text == "Swag Labs")
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail();
            }

        }



        [TearDown]
        public void TearDown()
        {

        }
    }
}